﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for SQLConnectionManager
/// </summary>
public class SQLConnectionManager
{
    //-------------------------------------------------------------------
    //- FIELDS                                                          -
    //-------------------------------------------------------------------
    SqlConnection connection;

    //-------------------------------------------------------------------
    //- PROPERTIES                                                      -
    //-------------------------------------------------------------------
    SqlConnection Connection
    {
        get
        {
            return connection;
        }
    }

    //-------------------------------------------------------------------
    //- METHODS                                                         -
    //-------------------------------------------------------------------
	public SQLConnectionManager()
	{
        connection = new SqlConnection();
	}

    //Opens a connection to the bitdev database
    public void Connect()
    {
        connection = new SqlConnection();

        connection.ConnectionString = "Data Source = bitdev.ict.op.ac.nz;" +
                                      "Initial Catalog = IN712_201401_MCPHEKT1;" +
                                      "User ID = MCPHEKT1;" +
                                      "Password = KMc_9DAA;";

        connection.Open();
    }

    //Closes the database connection
    public void Close()
    {
        connection.Close();
    }

    //Runs the given non select query against the database
    public int RunNonSelectQuery(string queryString)
    {
        try
        {
            SqlCommand command = new SqlCommand();
            command.Connection = connection;

            command.CommandText = queryString;

            int rowsAffected = command.ExecuteNonQuery();

            return rowsAffected;
        }
        catch (Exception e)
        {
            return -1;
        }
    }

    //Executes the given SELECT query and returns the SqlDataReader
    public SqlDataReader RunSelectQuery(string queryString)
    {
        try
        {
            SqlCommand command = new SqlCommand();
            command.Connection = connection;

            command.CommandText = queryString;

            SqlDataReader results = command.ExecuteReader();

            return results;
        }
        catch (Exception e)
        {
            return null;
        }
    }
}