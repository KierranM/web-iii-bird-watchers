﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

/// <summary>
/// Static class that has a method for creating a table from an SqlDataReader
/// </summary>
public static class TableMaker
{
    //-------------------------------------------------------------------
    //- METHODS                                                         -
    //-------------------------------------------------------------------
    public static Table Tablefy(SqlDataReader data, string tableWidth, string borderStyle, bool borderCollapse)
    {
        Table returnTable = new Table();

        returnTable.Style["border"] = borderStyle;
        returnTable.Style["width"] = tableWidth;
        if (borderCollapse)
        {
            returnTable.Style["border-collapse"] = "collapse";
        }

        //Verify that the data exists
        if (data != null)
        {
            //Create the header row
            TableHeaderRow headerRow = new TableHeaderRow();
            //Loop for every field in data and add the field name to the header row
            for (int i = 0; i < data.FieldCount; i++)
            {
                TableHeaderCell field = new TableHeaderCell();
                field.Text = data.GetName(i);
                field.Style["border"] = borderStyle;
                if (borderCollapse)
                {
                    field.Style["border-collapse"] = "collapse";
                }
                headerRow.Controls.Add(field);
            }

            //Add the header row to the table\
            returnTable.Controls.Add(headerRow);

            //Now loop for all the result sets and add them to the database
            while (data.Read())
            {
                //Create the row for the entire record
                TableRow row = new TableRow();

                for (int i = 0; i < data.FieldCount; i++)
                {
                    //Create the table cell for the singular field
                    TableCell cell = new TableCell();

                    //Get the value from the data
                    cell.Text = data.GetValue(i).ToString();
                    cell.Style["border"] = borderStyle;
                    if (borderCollapse)
                    {
                        cell.Style["border-collapse"] = "collapse";
                    }
                    row.Controls.Add(cell);
                }

                //Add the row to the table
                returnTable.Controls.Add(row);
            }

            //return the table
            return returnTable;
        }
        else
        {
            throw new ArgumentNullException("data", "Attempted to pass null SqlDataReader to TableMaker.Tablefy");
        }
    }
}