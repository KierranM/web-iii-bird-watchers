﻿<%@ Page Title="Members" Language="C#" AutoEventWireup="true" CodeFile="Members.aspx.cs" Inherits="Members" MasterPageFile="~/Site.master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <form runat=server>
        <div id="content" runat="server"
            style="background-color: rgba(255,255,255,0.8); border-radius: 10px; border: 1px solid black; overflow: hidden; padding: 10px;">
            <h1>Members</h1>
            <p>
                All the birders in our collective can be found below. If you wish to register as 
                a member send an email to <a href="mailto:bitbirders@gmail.com">BITBirders@gmail.com</a></p>
            <br />
            
        </div>
    </form>
</asp:Content>