﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <form runat="server">
        <div id="top10" 
            style="margin-right: 10px; width: 23%; height: 420px; border: 1px solid black; float: right; background-color: rgba(255, 255, 255, 0.9);border-radius: 10px;">
            <h3 align="center" style="font-weight: bold;">Top 10 Birders</h3>
            <br />
            <asp:GridView ID="topTenGrid" runat="server" Height="300px" Width="213px" 
                ForeColor="Black" HorizontalAlign="Center">
                <EditRowStyle HorizontalAlign="Center" />
                <EmptyDataRowStyle HorizontalAlign="Center" />
                <RowStyle HorizontalAlign="Center" BorderStyle="None" />
            </asp:GridView>
        </div>
        <div id="pageContent" 
            style="margin: 10px; top: 10px; width: 70%; height: 400px; background-color: rgba(255, 255, 255, 0.8); border-radius: 10px; border: 1px solid black; padding: 10px;">
        <br /> 
        <br />
            <h3 style="font-family: Arial, Helvetica, sans-serif; font-size: large;">Welcome to the BIT Bird Watchers Club</h3>
            <p>
            Birdwatching or birding is the observation of birds as a recreational activity. It can be done with the naked eye, through a visual enhancement device like binoculars and telescopes, or by listening for bird sounds.

    Birdwatching often involves a significant auditory component, as many bird species are more easily detected and identified by ear than by eye. 
                &nbsp;</p>
            <p>
                The BIT bird watchers club is a social collective of birders, this website is dedicated 
                to allowing us to keep a track of the birds that members have seen</p>
        </div>
    </form>
</asp:Content>
