﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class ReportSighting : System.Web.UI.Page
{
    protected enum ESightingEntryFormPart
    {
        PickMember,
        ConfirmMember,
        PickBird,
        ConfirmOrEnterNewBird,
        ConfirmSightingEntry,
        Success
    }

    private SQLConnectionManager connman;

    protected void Page_Load(object sender, EventArgs e)
    {

        //Create the connection manager
        connman = new SQLConnectionManager();

        //If the form has not been started set it to display the first entry form
        if(Session["sightingEntryPart"] == null)
        {
            //Set it to the start value
            Session["sightingEntryPart"] = 0;
        }

        //Retrieve the form that should be shown
        ESightingEntryFormPart formToShow = (ESightingEntryFormPart)Session["sightingEntryPart"];
        
        //Switch on the possible values of sightingEntryPart, which are represented by the enum ESightingEntryFormPart
        switch (formToShow)
        {
            case ESightingEntryFormPart.PickMember:
                ShowPickMemberForm();
                break;
            case ESightingEntryFormPart.ConfirmMember:
                ShowMemberConfirmation();
                break;
            case ESightingEntryFormPart.PickBird:
                ShowPickBirdForm();
                break;
            case ESightingEntryFormPart.ConfirmOrEnterNewBird:
                if (CheckBirdExists())
                {
                    ShowBirdConfirmation();
                }
                else
                {
                    ShowNewBirdEntryForm();
                }
                break;
            case ESightingEntryFormPart.ConfirmSightingEntry:
                ShowSightingConfirmation();
                break;
            case ESightingEntryFormPart.Success:
                ShowSuccessForm();
                break;
            default:
                ShowError();
                break;
        }
    }

    private void ShowError()
    {
        Label errorLabel = new Label();

        errorLabel.Text = "Something went wrong. Please refresh the page";

        //abandon the session
        Session.Abandon();
    }

    private void ShowSuccessForm()
    {
        //Display the title
        Label lblTitle = new Label();
        lblTitle.Text = "Sighting Added Successfully<br><br>";
        lblTitle.Style["font-size"] = "24px";
        addSightingForm.Controls.Add(lblTitle);
        Button btnFinish = new Button();
        btnFinish.Text = "Finish";
        addSightingForm.Controls.Add(btnFinish);

        Session.Abandon();
    }

    private void ShowSightingConfirmation()
    {
        //Display the title
        Label lblTitle = new Label();
        lblTitle.Text = "Confirm Sighting<br><br>";
        lblTitle.Style["font-size"] = "24px";
        addSightingForm.Controls.Add(lblTitle);

        int memberID = (int)Session["confirmedMemberID"];
        int birdID = (int)Session["confirmedBirdID"];

        //Select the details of the member
        string selectMember = string.Format("SELECT * FROM tblMember WHERE MemberID = '{0}'", memberID);
        string selectBird = string.Format("SELECT * FROM tblBird WHERE BirdID = '{0}'", birdID);

        connman.Connect();

        SqlDataReader memberResult = connman.RunSelectQuery(selectMember);

        memberResult.Read();
        string firstName = (string)memberResult["FirstName"];
        string lastName = (string)memberResult["LastName"];

        connman.Close();

        connman.Connect();
        SqlDataReader birdResult = connman.RunSelectQuery(selectBird);
        birdResult.Read();
        
        string englishName = (string)birdResult["EnglishName"];

        connman.Close();

        Label lblMessage = new Label();
        lblMessage.Text = string.Format("Please confirm this sighting:<br><b>{0} {1} seen a {2}</b><br>If this is correct click 'Next' to finalize this sighting<br><br>", firstName, lastName, englishName);
        addSightingForm.Controls.Add(lblMessage);

        //Close the connection
        connman.Close();

        AddPreviousButton();
        AddNextButton();
    }

    private void ShowNewBirdEntryForm()
    {
        //Set the flag in the Session variable indicating a new bird should be added
        Session["newBird"] = true;

        //Display the title
        Label lblTitle = new Label();
        lblTitle.Text = "Bird Not Found<br><br>";
        lblTitle.Style["font-size"] = "24px";
        addSightingForm.Controls.Add(lblTitle);

        //Display a message
        Label lblMessage = new Label();
        lblMessage.Text = "If you would like to add this bird enter its details below, otherwise click back and search again<br><br>";
        addSightingForm.Controls.Add(lblMessage);

        //Add the English name entry textbox
        Label lblEnglishName = new Label();
        lblEnglishName.Text = "English Name: ";
        TextBox txtEnglishName = new TextBox();
        txtEnglishName.ID = "txtEnglishName";
        addSightingForm.Controls.Add(lblEnglishName);
        addSightingForm.Controls.Add(txtEnglishName);

        //If the english name entry error flag exists
        if (Session["englishNameEntryError"] != null)
        {
            Label lblEntryError = new Label();
            lblEntryError.Text = "  The name cannot be empty or contain numbers";
            lblEntryError.Style["color"] = "red";
            addSightingForm.Controls.Add(lblEntryError);

            //remove the name entry failure flag
            Session["englishNameEntryError"] = null;
        }

        //Add the Maori name entry textbox
        Label lblMaoriName = new Label();
        lblMaoriName.Text = "<br>Maori Name: ";
        TextBox txtMaoriName = new TextBox();
        txtMaoriName.ID = "txtMaoriName";
        addSightingForm.Controls.Add(lblMaoriName);
        addSightingForm.Controls.Add(txtMaoriName);

        //If the english name entry error flag exists
        if (Session["maoriNameEntryError"] != null)
        {
            Label lblEntryError = new Label();
            lblEntryError.Text = "  The name cannot be empty or contain numbers";
            lblEntryError.Style["color"] = "red";
            addSightingForm.Controls.Add(lblEntryError);

            //remove the name entry failure flag
            Session["maoriNameEntryError"] = null;
        }

        //Add the Scientific name entry textbox
        Label lblScientificName = new Label();
        lblScientificName.Text = "<br>Scientific Name: ";
        TextBox txtScientificName = new TextBox();
        txtScientificName.ID = "txtScientificName";
        addSightingForm.Controls.Add(lblScientificName);
        addSightingForm.Controls.Add(txtScientificName);

        //If the english name entry error flag exists
        if (Session["scientificNameEntryError"] != null)
        {
            Label lblEntryError = new Label();
            lblEntryError.Text = "  The name cannot be empty or contain numbers";
            lblEntryError.Style["color"] = "red";
            addSightingForm.Controls.Add(lblEntryError);

            //remove the name entry failure flag
            Session["scientificNameEntryError"] = null;
        }

        Label lblBreakRules = new Label();
        lblBreakRules.Text = "<br><br>";
        addSightingForm.Controls.Add(lblBreakRules);

        AddPreviousButton();
        AddNextButton();
    }

    private void ShowBirdConfirmation()
    {
        //Display the title
        Label lblTitle = new Label();
        lblTitle.Text = "Confirm Bird<br><br>";
        lblTitle.Style["font-size"] = "24px";
        addSightingForm.Controls.Add(lblTitle);

        //Select all the matched birds
        List<string> matchedBirds = SelectBirds();

        //Display something different if only one bird matches
        if (matchedBirds.Count == 1)
        {
            Session["singleBird"] = true;

            string[] member = matchedBirds[0].Split(',');

            Label lblConfirmation = new Label();
            lblConfirmation.Text = string.Format("Is <b>{0}, {1}, {2}</b> correct?<br>If so click next, otherwise click back<br><br>", member[1], member[2], member[3]);
            addSightingForm.Controls.Add(lblConfirmation);
        }
        else //There were multiple birds that matched
        {
            Session["singleBird"] = null;

            Label lblConfirmation = new Label();
            lblConfirmation.Text = "The following birds match that name: <br>";
            addSightingForm.Controls.Add(lblConfirmation);

            //Create a RadioButtonList to show the user options
            RadioButtonList rbListBirdOptions = new RadioButtonList();
            rbListBirdOptions.ID = "rbListBirdOptions";

            //Loop for every bird in the list
            foreach (string bird in matchedBirds)
            {
                string[] birdDetails = bird.Split(',');
                string itemText = string.Format("<b>{0}, {1}, {2}</b>", birdDetails[1], birdDetails[2], birdDetails[3]);

                ListItem rbItem = new ListItem(itemText, birdDetails[0]);

                rbListBirdOptions.Items.Add(rbItem);
            }

            //If it a selected index is already present in the Session variable
            if (Session["selectedBirdIndex"] != null)
            {
                rbListBirdOptions.SelectedIndex = (int)Session["selectedBirdIndex"];
            }
            else
            {
                //Set the default selected index
                rbListBirdOptions.SelectedIndex = 0;
            }

            addSightingForm.Controls.Add(rbListBirdOptions);
        }

        Label lblBreakRules = new Label();
        lblBreakRules.Text = "<br><br>";
        addSightingForm.Controls.Add(lblBreakRules);

        AddPreviousButton();
        AddNextButton();
    }

    private bool CheckBirdExists()
    {
        bool exists = false;

        List<string> results = SelectBirds();

        if (results.Count > 0)
        {
            exists = true;
        }

        return exists;    
    }

    private List<string> SelectBirds()
    {
        //Create the return list
        List<string> resultList = new List<string>();

        //Connect to the database
        connman.Connect();

        //Start constructing the select query
        string selectString = "SELECT DISTINCT BirdID, EnglishName, MaoriName, ScientificName FROM tblBird WHERE ";

        //Retrieve the entered name from Session
        string name = Session["enteredBird"].ToString();

        //Add the first part of the where statement of the query
        selectString += string.Format("EnglishName LIKE '%{0}%' OR MaoriName LIKE '%{0}%' OR ScientificName LIKE '%{0}%'", name);

        //finaly add the grouping and ordering to the query
        selectString += " ORDER BY EnglishName";

        SqlDataReader results = connman.RunSelectQuery(selectString);

        //If there were any results returned
        if (results.HasRows)
        {
            while (results.Read())
            {
                //Create a comma delimited string representing the row
                string row = string.Format("{0},{1},{2},{3}", results["BirdID"], results["EnglishName"], results["MaoriName"], results["ScientificName"]);
                resultList.Add(row);
            }
        }

        //Close the connection
        connman.Close();
        return resultList;
    }

    private void ShowPickBirdForm()
    {
        Label lblTitle = new Label();
        lblTitle.Text = "Which Bird was seen?<br><br>";
        lblTitle.Style["font-size"] = "24px";
        addSightingForm.Controls.Add(lblTitle);

        Label lblBirdName = new Label();
        lblBirdName.Text = "Birds Name (English, Maori, or Scientific): ";
        addSightingForm.Controls.Add(lblBirdName);

        TextBox txtBirdName = new TextBox();

        //If there is already a value for the textbox in Session place it in the textbox
        if (Session["enteredBird"] != null)
        {
            txtBirdName.Text = (string)Session["enteredBird"];
        }

        //Give the text box an ID so we can retrieve it later
        txtBirdName.ID = "txtBirdName";
        addSightingForm.Controls.Add(txtBirdName);

        //If the name entry error flag exists
        if (Session["birdEntryFailure"] != null)
        {
            Label lblEntryError = new Label();
            lblEntryError.Text = "  Bird name cannot be empty or contain numbers";
            lblEntryError.Style["color"] = "red";
            addSightingForm.Controls.Add(lblEntryError);

            //remove the name entry failure flag
            Session["birdEntryFailure"] = null;
        }

        Label lblBreakRules = new Label();
        lblBreakRules.Text = "<br><br>";
        addSightingForm.Controls.Add(lblBreakRules);

        AddPreviousButton();
        AddNextButton();
    }

    private void ShowMemberConfirmation()
    {
        Label lblTitle = new Label();
        lblTitle.Text = "Confirm Member<br><br>";
        lblTitle.Style["font-size"] = "24px";
        addSightingForm.Controls.Add(lblTitle);

        //Select the members that match the entered name(s)
        List<string> matchedMembers = SelectMembers();

        //Show something different if there is only one member
        if (matchedMembers.Count == 1)
        {
            Session["singleMember"] = true;

            string[] member = matchedMembers[0].Split(',');

            Label lblConfirmation = new Label();
            lblConfirmation.Text = string.Format("Is <b>{0} {1}</b> from <b>{2}</b> correct?<br>If so click next, otherwise click back<br><br>", member[1], member[2], member[3]);
            addSightingForm.Controls.Add(lblConfirmation);
        }
        else //there were multiple members that matched
        {
            Session["singleMember"] = null;

            Label lblConfirmation = new Label();
            lblConfirmation.Text = "The following members match that name: <br>";
            addSightingForm.Controls.Add(lblConfirmation);

            //Create a RadioButtonList to show the user options
            RadioButtonList rbListMemberOptions = new RadioButtonList();
            rbListMemberOptions.ID = "rbListMemberOptions";

            //Loop for every member in the list
            foreach (string member in matchedMembers)
            {
                string[] memberDetails = member.Split(',');
                string itemText = string.Format("<b>{0} {1}</b> from <b>{2}</b>", memberDetails[1], memberDetails[2], memberDetails[3]);

                ListItem rbItem = new ListItem(itemText, memberDetails[0]);

                rbListMemberOptions.Items.Add(rbItem);
            }

            //If it a selected index is already present in the Session variable
            if(Session["selectedMemberIndex"] != null)
            {
                rbListMemberOptions.SelectedIndex = (int)Session["selectedMemberIndex"];
            }
            else
            {
                //Set the default selected index
                rbListMemberOptions.SelectedIndex = 0;
            }

            addSightingForm.Controls.Add(rbListMemberOptions);
        }

        Label lblBreak = new Label();
        lblBreak.Text = "<br>";
        addSightingForm.Controls.Add(lblBreak);

        AddPreviousButton();
        AddNextButton();
    }

    private bool CheckMemberExists()
    {
        bool exists = false;

        List<string> results = SelectMembers();

        if (results.Count > 0)
        {
            exists = true;
        }

        return exists;
    }

    private List<string> SelectMembers()
    {
        //Create the return list
        List<string> resultList = new List<string>();

        //Connect to the database
        connman.Connect();

        //Start constructing the select query
        string selectString = "SELECT DISTINCT MemberID, FirstName, LastName, Suburb FROM tblMember WHERE ";

        //Retrieve the entered name from Session
        string name = Session["enteredName"].ToString();

        string[] nameparts = name.Split(' ');

        string nameOne = nameparts[0];

        //Add the first part of the where statement of the query
        selectString += string.Format("FirstName LIKE '%{0}%' OR LastName LIKE '%{0}%'", nameOne);


        //If the string the user entered had two parts then check for both
        if (nameparts.Length == 2)
        {
            string nameTwo = nameparts[1];

            selectString += string.Format(" OR FirstName LIKE '%{0}%' OR LastName LIKE '%{0}%'", nameTwo);
        }

        //finaly add the grouping and ordering to the query
        selectString += " ORDER BY FirstName, LastName";

        SqlDataReader results = connman.RunSelectQuery(selectString);

        //If there were any results returned
        if (results.HasRows)
        {
            while (results.Read())
            {
                //Create a comma delimited string representing the row
                string row = string.Format("{0},{1},{2},{3}", results["MemberID"], results["FirstName"], results["LastName"], results["Suburb"]);
                resultList.Add(row);
            }
        }

        //Close the connection
        connman.Close();
        return resultList;
    }

    private void ShowPickMemberForm()
    {
        //Create the controls for this display and add them to the form

        Label lblFormTitle = new Label();
        lblFormTitle.Text = "Choose A Member<br><br>";
        lblFormTitle.Style["font-size"] = "24px";
        addSightingForm.Controls.Add(lblFormTitle);

        Label lblMemberName = new Label();
        lblMemberName.Text = "Members Name (firstname surname): ";
        addSightingForm.Controls.Add(lblMemberName);

        TextBox txtMemberName = new TextBox();

        //If there is already a value for the textbox in Session place it in the textbox
        if (Session["enteredName"] != null)
        {
            txtMemberName.Text = (string)Session["enteredName"];
        }

        //Give the text box an ID so we can retrieve it later
        txtMemberName.ID = "txtMemberName";
        addSightingForm.Controls.Add(txtMemberName);

        //If the name entry error flag exists
        if (Session["nameEntryFailure"] != null)
        {
            Label lblEntryError = new Label();
            lblEntryError.Text = "  Name cannot be empty or contain numbers";
            lblEntryError.Style["color"] = "red";
            addSightingForm.Controls.Add(lblEntryError);

            //remove the name entry failure flag
            Session["nameEntryFailure"] = null;
        }

        //If the member not found flag exists in session show a message
        if (Session["noMemberFound"] != null)
        {
            Label lblNoMember = new Label();
            lblNoMember.Text = "  No member found with that name";
            lblNoMember.Style["color"] = "red";
            addSightingForm.Controls.Add(lblNoMember);

            //remove the no member found flag
            Session["noMemberFound"] = null;
        }

        Label lblBreakRules = new Label();
        lblBreakRules.Text = "<br><br>";
        addSightingForm.Controls.Add(lblBreakRules);

        AddNextButton();
    }

    //Adds the next button to the form
    private void AddNextButton()
    {
        Button btnNext = new Button();
        btnNext.Text = "Next";
        btnNext.Click += new EventHandler(NextFormButtonClick);
        addSightingForm.Controls.Add(btnNext);
    }

    //Adds the previous button to the form
    private void AddPreviousButton()
    {
        Button btnPrev = new Button();
        btnPrev.Text = "Back";
        btnPrev.Style["margin-right"] = "50px";
        btnPrev.Click += new EventHandler(PreviousButtonClick);
        addSightingForm.Controls.Add(btnPrev);
    }

    //Gets a control from the form
    private Control GetControlFromForm(string controlID)
    {
        return addSightingForm.Controls.Cast<Control>().ToList().Find((Control c) => c.ID == controlID);
    }

    //Occurs when one of the Next buttons is clicked
    private void NextFormButtonClick(object sender, EventArgs e)
    {
        //Retrieve the form that is currently shown
        ESightingEntryFormPart currentForm = (ESightingEntryFormPart)Session["sightingEntryPart"];

        //the next form part number
        ESightingEntryFormPart nextForm = currentForm;

        //Switch on the possible values of sightingEntryPart, which are represented by the enum ESightingEntryFormPart
        switch (currentForm)
        {
            case ESightingEntryFormPart.PickMember:
                //Get the textbox from the forms control collection
                TextBox txtMemberName = (TextBox)GetControlFromForm("txtMemberName");

                //Make sure that the textbox has text in it and that none of the text is numbers
                if ((txtMemberName.Text.Length > 0) && ((txtMemberName.Text.Any<char>((x) => (char.IsNumber(x)))) == false))
                {
                    Session["enteredName"] = txtMemberName.Text;
                    if (CheckMemberExists())
                    {
                        nextForm = ESightingEntryFormPart.ConfirmMember;
                    }
                    else
                    {
                        Session["noMemberFound"] = true;
                        nextForm = ESightingEntryFormPart.PickMember;
                    }
                }
                else
                {
                    Session["nameEntryFailure"] = true;

                    //Set the next form to be this form again
                    nextForm = ESightingEntryFormPart.PickMember;
                }

                break;
            case ESightingEntryFormPart.ConfirmMember:
                int memberID;

                //The action differs between multiple members available and a single members
                if (Session["singleMember"] != null)
                {
                    List<string> member = SelectMembers();

                    string[] memberDetails = member[0].Split(',');

                    memberID = Convert.ToInt32(memberDetails[0]);
                }
                else //There were multiple options for members
                {
                    //Find the radiobuttonlist in the forms controls
                    RadioButtonList rbListMemberOptions = (RadioButtonList)GetControlFromForm("rbListMemberOptions");

                    memberID = Convert.ToInt32(rbListMemberOptions.SelectedValue);

                    Session["selectedMemberIndex"] = rbListMemberOptions.SelectedIndex;
                }

                Session["confirmedMemberID"] = memberID;

                nextForm = ESightingEntryFormPart.PickBird;
                break;
            case ESightingEntryFormPart.PickBird:
                //Get the textbox from the forms control collection
                TextBox txtBirdName = (TextBox)GetControlFromForm("txtBirdName");

                //Make sure that the textbox has text in it and that none of the text is numbers
                if ((txtBirdName.Text.Length > 0) && ((txtBirdName.Text.Any<char>((x) => (char.IsNumber(x)))) == false))
                {
                    Session["enteredBird"] = txtBirdName.Text;
                   
                    nextForm = ESightingEntryFormPart.ConfirmOrEnterNewBird;
                }
                else
                {
                    Session["birdEntryFailure"] = true;

                    //Set the next form to be this form again
                    nextForm = ESightingEntryFormPart.PickBird;
                }
                break;
            case ESightingEntryFormPart.ConfirmOrEnterNewBird:
                //If newBird is set in Session
                if (Session["newBird"] != null)
                {
                    TextBox txtEnglishName = (TextBox)GetControlFromForm("txtEnglishName");
                    TextBox txtMaoriName = (TextBox)GetControlFromForm("txtMaoriName"); 
                    TextBox txtScientificName = (TextBox)GetControlFromForm("txtScientificName");

                    //Check if each field is valid in turn
                    if ((txtEnglishName.Text.Length > 0) && ((txtEnglishName.Text.Any<char>((x) => (char.IsNumber(x)))) == false))
                    {
                        if ((txtMaoriName.Text.Length > 0) && ((txtMaoriName.Text.Any<char>((x) => (char.IsNumber(x)))) == false))
                        {
                            if ((txtScientificName.Text.Length > 0) && ((txtScientificName.Text.Any<char>((x) => (char.IsNumber(x)))) == false))
                            {
                                //All names valid so add the bird to the database
                                string englishName = txtEnglishName.Text;
                                string maoriName = txtMaoriName.Text;
                                string scientificName = txtScientificName.Text;

                                //Create the query string
                                string insertQuery = string.Format("INSERT INTO tblBird VALUES ('{0}', '{1}', '{2}')", maoriName, englishName, scientificName);

                                connman.Connect();

                                int success = connman.RunNonSelectQuery(insertQuery);

                                connman.Close();

                                //If a row was affected
                                if (success > 0)
                                {
                                    //Use the entered name to set the value in Session
                                    Session["enteredBird"] = englishName;

                                    //Select the BirdID of the new bird
                                    string selectString = string.Format("SELECT EnglishName FROM tblBird WHERE EnglishName = '{0}' AND MaoriName = '{1}' AND ScientificName = '{2}'", englishName, maoriName, scientificName);

                                    connman.Connect();
                                    SqlDataReader result = connman.RunSelectQuery(selectString);
                                    result.Read();

                                    Session["enteredBird"] = result["EnglishName"];
                                    
                                    //Now that the bird is added remove the new bird flag
                                    Session["newBird"] = null;

                                    connman.Close();
                                    nextForm = ESightingEntryFormPart.ConfirmOrEnterNewBird; 
                                }
                                else //The insertion was unsuccessful
                                {
                                    ShowError();
                                }
                            }
                            else //Scientific name was invalid
                            {
                                Session["scientificNameEntryError"] = true;
                            }
                        }
                        else //Maori name was invalid
                        {
                            Session["maoriNameEntryError"] = true;
                        }
                    }
                    else //English name was invalid
                    {
                        Session["englishNameEntryError"] = true;
                    }
                }
                else //We are not adding a new bird
                {
                    int birdID;

                    //The action differs between multiple birds available and a single birds
                    if (Session["singleBird"] != null)
                    {
                        List<string> bird = SelectBirds();

                        string[] birdDetails = bird[0].Split(',');

                        birdID = Convert.ToInt32(birdDetails[0]);
                    }
                    else //There were multiple options for birds
                    {
                        //Find the radiobuttonlist in the forms controls
                        RadioButtonList rbListBirdOptions = (RadioButtonList)GetControlFromForm("rbListBirdOptions");

                        birdID = Convert.ToInt32(rbListBirdOptions.SelectedValue);

                        Session["selectedBirdIndex"] = rbListBirdOptions.SelectedIndex;
                    }

                    Session["confirmedBirdID"] = birdID;

                    nextForm = ESightingEntryFormPart.ConfirmSightingEntry; 
                }
                break;
            case ESightingEntryFormPart.ConfirmSightingEntry:
                //Retrieve the ids from Session
                int idOfMember = (int)Session["confirmedMemberID"];
                int idOfBird = (int)Session["confirmedBirdID"];

                //Insert the sighting into the tblBirdMember table
                string insert = string.Format("INSERT INTO tblBirdMember VALUES ({0}, {1})", idOfBird, idOfMember);

                //Execute the query
                connman.Connect();
                int rowsAffected = connman.RunNonSelectQuery(insert);
                connman.Close();

                if (rowsAffected > 0)
                {
                    nextForm = ESightingEntryFormPart.Success;
                }
                else
                {
                    ShowError();
                }

                break;
            default:
                ShowError();
                break;
        }

        //Increase the form value so that it shows the next form
        Session["sightingEntryPart"] = (int)nextForm;

        Response.Redirect(Request.RawUrl);
    }

    void PreviousButtonClick(object sender, EventArgs e)
    {
        //Retrieve the form that is currently shown
        ESightingEntryFormPart currentForm = (ESightingEntryFormPart)Session["sightingEntryPart"];

        ESightingEntryFormPart previousForm = currentForm;

        switch (currentForm)
        {
            case ESightingEntryFormPart.PickMember:
                //Do nothing
                break;
            case ESightingEntryFormPart.ConfirmMember:
                previousForm = ESightingEntryFormPart.PickMember;
                break;
            case ESightingEntryFormPart.PickBird:
                previousForm = ESightingEntryFormPart.ConfirmMember;
                break;
            case ESightingEntryFormPart.ConfirmOrEnterNewBird:
                previousForm = ESightingEntryFormPart.PickBird;
                break;
            case ESightingEntryFormPart.ConfirmSightingEntry:
                previousForm = ESightingEntryFormPart.ConfirmOrEnterNewBird;
                break;
        }

        Session["sightingEntryPart"] = (int)previousForm;
        Response.Redirect(Request.RawUrl);
    }
}