﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Sightings : System.Web.UI.Page
{
    SQLConnectionManager connman;

    protected void Page_Load(object sender, EventArgs e)
    {
        connman = new SQLConnectionManager();

        ShowMemberSightings();
        ShowBirdSightings();
    }

    private void ShowMemberSightings()
    {
        //Create the select string
        string selectString = "SELECT (dbo.tblMember.FirstName + ' ' + dbo.tblMember.LastName) AS 'Member Name', dbo.tblBird.EnglishName AS 'Birds English Name', dbo.tblBird.MaoriName AS ' Birds Maori Name' FROM dbo.tblBird JOIN dbo.tblBirdMember ON tblBird.BirdID = tblBirdMember.BirdID JOIN dbo.tblMember ON tblMember.MemberID = tblBirdMember.MemberID GROUP BY tblMember.LastName, tblMember.FirstName, tblBird.EnglishName, tblBird.MaoriName";

        //open the connection
        connman.Connect();

        //Execute the query and get the result set
        SqlDataReader results = connman.RunSelectQuery(selectString);

        if (results != null)
        {
            Table tabledData = TableMaker.Tablefy(results, "100%", "none", true);




            //Style the table

            tabledData.Rows[0].Style["border-bottom"] = "1px solid black";
            tabledData.Rows[0].Style["font-size"] = "12px";

            foreach (TableRow row in tabledData.Rows)
            {
                //Ignore the header row
                if (tabledData.Rows.GetRowIndex(row) > 0)
                {

                    //Style all cells
                    foreach (TableCell cell in row.Cells)
                    {
                        cell.Style["padding-top"] = "10px";
                        cell.Style["border-bottom"] = "2px solid rgba(0,0,255,0.3)";
                        cell.Style["font-size"] = "12px";
                    }
                }
            }

            memberSightings.Controls.Add(tabledData);
        }

        connman.Close();
    }

    private void ShowBirdSightings()
    {
        //Create the select string
        string selectString = "SELECT COUNT(*) AS 'Times Sighted', dbo.tblBird.EnglishName AS 'English Name', dbo.tblBird.MaoriName AS 'Maori Name' FROM dbo.tblBird JOIN dbo.tblBirdMember ON tblBird.BirdID = tblBirdMember.BirdID JOIN dbo.tblMember ON tblMember.MemberID = tblBirdMember.MemberID GROUP BY tblBird.EnglishName, tblBird.MaoriName ORDER BY 'Times Sighted' DESC";

        //open the connection
        connman.Connect();

        //Execute the query and get the result set
        SqlDataReader results = connman.RunSelectQuery(selectString);

        if (results != null)
        {
            Table tabledData = TableMaker.Tablefy(results, "100%", "none", true);




            //Style the table

            tabledData.Rows[0].Style["border-bottom"] = "1px solid black";
            tabledData.Rows[0].Style["font-size"] = "12px";

            foreach (TableRow row in tabledData.Rows)
            {
                //Ignore the header row
                if (tabledData.Rows.GetRowIndex(row) > 0)
                {
                    row.Cells[0].Style["text-align"] = "center";

                    //Style all cells
                    foreach (TableCell cell in row.Cells)
                    {
                        cell.Style["padding-top"] = "10px";
                        cell.Style["border-bottom"] = "2px solid rgba(0,0,255,0.3)";
                        cell.Style["font-size"] = "12px";
                    }
                }
            }

            birdSightings.Controls.Add(tabledData);
        }

        connman.Close();
    }
}
