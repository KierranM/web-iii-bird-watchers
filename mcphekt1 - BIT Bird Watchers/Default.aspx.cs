﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class _Default : System.Web.UI.Page
{
    SQLConnectionManager connman;

    protected void Page_Load(object sender, EventArgs e)
    {
        connman = new SQLConnectionManager();

        //Populate the top 10 table
        PopulateTopTen();
    }

    private void PopulateTopTen()
    {
        connman.Connect();

        string selectString = "SELECT TOP 10 COUNT(*) AS Sightings, (dbo.tblMember.FirstName + ' ' + dbo.tblMember.LastName) AS Name FROM dbo.tblBird JOIN dbo.tblBirdMember ON tblBird.BirdID = tblBirdMember.BirdID JOIN dbo.tblMember ON tblMember.MemberID = tblBirdMember.MemberID GROUP BY FirstName, LastName ORDER BY Sightings DESC";

        SqlDataReader results = connman.RunSelectQuery(selectString);

        if (results != null)
        {
            topTenGrid.DataSource = results;
            topTenGrid.DataBind();
        }

        connman.Close();
    }
}
