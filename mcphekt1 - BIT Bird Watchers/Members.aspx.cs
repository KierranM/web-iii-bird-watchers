﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Members : System.Web.UI.Page
{
    SQLConnectionManager connman;

    protected void Page_Load(object sender, EventArgs e)
    {
        connman = new SQLConnectionManager();

        ShowMembers();
    }

    private void ShowMembers()
    {
        //Create the select string
        string selectString = "SELECT COUNT(*) AS 'Birds Sighted', (dbo.tblMember.FirstName + ' ' + dbo.tblMember.LastName) AS 'Name', tblMember.Suburb FROM dbo.tblBird JOIN dbo.tblBirdMember ON tblBird.BirdID = tblBirdMember.BirdID JOIN dbo.tblMember ON tblMember.MemberID = tblBirdMember.MemberID GROUP BY tblMember.LastName, tblMember.FirstName, tblMember.Suburb";

        //open the connection
        connman.Connect();

        //Execute the query and get the result set
        SqlDataReader results = connman.RunSelectQuery(selectString);

        if (results != null)
        {
            Table tabledData = TableMaker.Tablefy(results, "100%" ,"none", true);

           


            //Style the table

            tabledData.Rows[0].Style["border-bottom"] = "1px solid black";
            tabledData.Rows[0].Style["font-size"] = "16px";

            foreach (TableRow row in tabledData.Rows)
            {
                //Ignore the header row
                if (tabledData.Rows.GetRowIndex(row) > 0)
                {
                    row.Cells[0].Style["text-align"] = "center";

                    //Style all cells
                    foreach (TableCell cell in row.Cells)
                    {
                        cell.Style["padding-top"] = "10px";
                        cell.Style["border-bottom"] = "2px solid rgba(0,0,255,0.3)";
                        cell.Style["font-size"] = "16px";
                    } 
                }
            }
            
            content.Controls.Add(tabledData);
        }

        connman.Close();
    }
}