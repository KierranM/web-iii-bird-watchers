﻿<%@ Page Title="Sightings" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Sightings.aspx.cs" Inherits="Sightings" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <form runat="server">
        <div id="content" 
            style="background-color: rgba(255,255,255,0.8); border-radius: 10px; border: 1px solid black; overflow: hidden; padding-left: 10px;">
            <h1>Sightings</h1>
            <p>Have a look at some of our members sightings, or look at all the birds our collective has seen</p>
            <div id='memberSightings' runat="server" style="width: 45%; float: left; margin-bottom: 20px;">
                <h1 style="padding-left: 10px;">Member Sightings</h1>
                <asp:GridView ID="gridMemberSightings" runat="server" ForeColor="#333333" 
                    style="position: relative; top: 4px; left: 12px" width="400px">
                </asp:GridView>
                <br />
            </div>
            <div id='birdSightings' runat="server" style="width: 50%; float: right; margin-right: 10px; margin-bottom: 20px;">
                <h1>&nbsp;&nbsp;Bird Sightings</h1>
                <asp:GridView ID="gridBirdSightings" runat="server" ForeColor="#333333" 
                    style="position: relative; top: 4px; left: 12px" width="400px">
                </asp:GridView>
                <br />
            </div>
        </div>
    </form>
</asp:Content>
