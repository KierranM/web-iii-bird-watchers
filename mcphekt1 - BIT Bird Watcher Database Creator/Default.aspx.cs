﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class _Default : System.Web.UI.Page
{
    SqlConnection bitdevConn;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //Opens a connection to the bitdev database
    private void Connect()
    {
        bitdevConn = new SqlConnection();

        bitdevConn.ConnectionString = "Data Source = bitdev.ict.op.ac.nz;" +
                                      "Initial Catalog = IN712_201401_MCPHEKT1;" +
                                      "User ID = MCPHEKT1;" +
                                      "Password = KMc_9DAA;";

        bitdevConn.Open();
    }

    //Runs the given non select query against the database
    private int RunNonSelectQuery(string queryString)
    {
        try
        {
            SqlCommand command = new SqlCommand();
            command.Connection = bitdevConn;

            command.CommandText = queryString;

            int rowsAffected = command.ExecuteNonQuery();

            return rowsAffected;
        }
        catch (Exception e)
        {
            //If the query causes an error, display it
            ShowErrorLabel("Non-Query Error: " + queryString + "<br>" + e.Message);
            return -1;
        }
    }

    //Executes the given SELECT query and returns the SqlDataReader
    private SqlDataReader RunSelectQuery(string queryString)
    {
        try
        {
            SqlCommand command = new SqlCommand();
            command.Connection = bitdevConn;

            command.CommandText = queryString;

            SqlDataReader results = command.ExecuteReader();

            return results;
        }
        catch (Exception e)
        {
            //If the query causes an error, display it
            ShowErrorLabel("Select Query Error: " + queryString + "<br>" + e.Message);
            return null;
        }
    }

    //Displays the given message in a new label that is appened to the end of the page
    private void ShowErrorLabel(string errorMessage)
    {
        Label errorLabel = new Label();
        errorLabel.Text = errorMessage + "<br>";
        this.Controls.Add(errorLabel);
    }

    //Drops a table with the given name from the database
    private void DropTable(string tableName)
    {
        Connect();
        string destroyString = "IF OBJECT_ID('" + tableName + "', 'U') IS NOT NULL DROP TABLE dbo." + tableName + ";";
        RunNonSelectQuery(destroyString);
        bitdevConn.Close();
    }


    //Table Creation Methods, Creates the tables required for the database
    private void CreateTableBird()
    {
        string create = "CREATE TABLE tblBird (BirdID INT IDENTITY, MaoriName VARCHAR(100) NOT NULL, EnglishName VARCHAR(100) NOT NULL, ScientificName VARCHAR(100) NOT NULL, PRIMARY KEY (BirdID));";

        RunNonSelectQuery(create);
    }

    private void CreateTableMember()
    {
        string create = "CREATE TABLE tblMember (MemberID INT IDENTITY, LastName VARCHAR(30) NOT NULL, FirstName VARCHAR(30) NOT NULL, Suburb VARCHAR(30) NOT NULL, PRIMARY KEY (MemberID));";

        RunNonSelectQuery(create);
    }

    private void CreateTableBirdMember()
    {
        string create = "CREATE TABLE tblBirdMember (BirdID INT NOT NULL, MemberID INT NOT NULL, FOREIGN KEY (BirdID) REFERENCES tblBird(BirdID), FOREIGN KEY (MemberID) REFERENCES tblMember(MemberID));";

        RunNonSelectQuery(create);
    }

    //Insert into table methods, inserts individual records into the different tables of the database
    //Returns true if at least one row was affected
    private bool InsertBirdRecord(string maoriName, string englishName, string scientificName)
    {
        string insertQuery = string.Format("INSERT INTO dbo.tblBird VALUES ('{0}', '{1}', '{2}');", maoriName, englishName, scientificName);

        int result = RunNonSelectQuery(insertQuery);

        //If the number of affected rows was a number greater than 0 then return true
        if (result > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private bool InsertMemberRecord(string lastName, string firstName, string suburb)
    {
        string insertQuery = string.Format("INSERT INTO dbo.tblMember VALUES ('{0}', '{1}', '{2}');", lastName, firstName, suburb);

        int result = RunNonSelectQuery(insertQuery);

        //If the number of affected rows was a number greater than 0 then return true
        if (result > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private bool InsertBirdMemberRecord(int birdID, int memberID)
    {
        string insertQuery = string.Format("INSERT INTO dbo.tblBirdMember VALUES ({0}, {1});", birdID, memberID);

        int result = RunNonSelectQuery(insertQuery);

        //If the number of affected rows was a number greater than 0 then return true
        if (result > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //Seed methods, fills the database with dummy data
    private void SeedBirds()
    {

        Connect();

        //Create a label and gridview for displaying success information
        Label birdLabel = new Label();
        birdLabel.Text = "</br>Birds Added</br>";

        GridView birdGrid = new GridView();

        form1.Controls.Add(birdLabel);
        form1.Controls.Add(birdGrid);

        InsertBirdRecord("Kereru", "New Zealand Wood Pigeon", "Hemiphaga novaeseelandiae");
        InsertBirdRecord("Korimako", "Bell bird", "Anthornis melanura");
        InsertBirdRecord("Piwakawaka", "Fantail", "Rhipidura fulginosa");
        InsertBirdRecord("Tauhou", "Silvereye", "Zosterops lateralis");
        InsertBirdRecord("Toroa", "Royal Albatross", "Diomedea epomophora");
        InsertBirdRecord("Tui", "Parson Bird", "Prosthemadera novaeseelandiae");
        InsertBirdRecord("Wani", "Black Swan", "Cygnus atratus");

        //Select all of the birds that were just inserted
        string selectAllBirds = "SELECT * FROM dbo.tblBird";
        SqlDataReader results = RunSelectQuery(selectAllBirds);

        //if anything was returned display it to the gridview
        if (results != null)
        {
            birdGrid.DataSource = results;
            birdGrid.DataBind();
        }

        bitdevConn.Close();
    }

    private void SeedMembers()
    {

        Connect();

        //Create a label and gridview for displaying success information
        Label memberLabel = new Label();
        memberLabel.Text = "</br>Members Added</br>";

        GridView memberGrid = new GridView();

        form1.Controls.Add(memberLabel);
        form1.Controls.Add(memberGrid);

        InsertMemberRecord("McCormack", "Howard", "Pine Hill");
        InsertMemberRecord("Kerford", "Claudia", "Dunedin North");
        InsertMemberRecord("Curro", "Benita", "St. Kilda");
        InsertMemberRecord("Felsch", "Eva", "Roslyn");
        InsertMemberRecord("Vandine", "Erik", "Opoho");
        InsertMemberRecord("Moroney", "Louisa", "Ravensbourne");
        InsertMemberRecord("Loh", "Jessie", "Waverly");
        InsertMemberRecord("Stanford", "Ngaio", "Opoho");
        InsertMemberRecord("Mills", "Elva", "Roslyn");
        InsertMemberRecord("Woodford", "Sacha", "St. Kilda");

        //Select all of the members that were just inserted
        string selectAllMembers = "SELECT * FROM dbo.tblMember";
        SqlDataReader results = RunSelectQuery(selectAllMembers);

        //if anything was returned display it to the gridview
        if (results != null)
        {
            memberGrid.DataSource = results;
            memberGrid.DataBind();
        }

        bitdevConn.Close();
    }

    private void SeedBirdMembers()
    {
        Connect();

        //Create a label and gridview for displaying success information
        Label memberBirdLabel = new Label();
        memberBirdLabel.Text = "</br>Member-Birds Added</br>";

        GridView memberBirdGrid = new GridView();

        form1.Controls.Add(memberBirdLabel);
        form1.Controls.Add(memberBirdGrid);

        InsertBirdMemberRecord(1, 2);
        InsertBirdMemberRecord(1, 3);
        InsertBirdMemberRecord(1, 7);
        InsertBirdMemberRecord(2, 5);
        InsertBirdMemberRecord(4, 9);
        InsertBirdMemberRecord(6, 5);
        InsertBirdMemberRecord(5, 10);
        InsertBirdMemberRecord(6, 9);
        InsertBirdMemberRecord(4, 7);
        InsertBirdMemberRecord(3, 2);
        InsertBirdMemberRecord(5, 8);
        InsertBirdMemberRecord(6, 7);
        InsertBirdMemberRecord(4, 10);
        InsertBirdMemberRecord(6, 1);
        InsertBirdMemberRecord(2, 4);
        InsertBirdMemberRecord(3, 6);

        //Select all of the bird-members that were just inserted
        string selectAllMemberBirds = "SELECT tblBird.EnglishName, tblMember.LastName FROM tblBird JOIN dbo.tblBirdMember ON tblBird.BirdID = tblBirdMember.BirdID JOIN tblMember ON tblMember.MemberID = tblBirdMember.MemberID";
        SqlDataReader results = RunSelectQuery(selectAllMemberBirds);

        //if anything was returned display it to the gridview
        if (results != null)
        {
            memberBirdGrid.DataSource = results;
            memberBirdGrid.DataBind();
        }

        bitdevConn.Close();
    }

    //The DB creation method
    private void CreateBirdWatchersDatabase()
    {
        //Drop all the tables
        DropTable("tblBirdMember");
        DropTable("tblBird");
        DropTable("tblMember");

        Connect();
        //Recreate the three tables
        CreateTableBird();
        CreateTableMember();
        CreateTableBirdMember();
        bitdevConn.Close();

        //Seed the dummy data into the tables
        SeedBirds();
        SeedMembers();
        SeedBirdMembers();
    }

    protected void btnNuke_Click(object sender, EventArgs e)
    {
        CreateBirdWatchersDatabase();
    }
}

